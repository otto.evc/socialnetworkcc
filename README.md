### Social Network Friend Finder

This is a simple program that prints
if 2 users are connected on a mock social network by their friends list.

go version = 1.13.1

#### HOW TO RUN

Download the code using git or zip and make sure you add it in your $GOPATH/src
it should look like this

`$GOPATH/src/socialnetworkcc`

*  run `go build`

*  run `go install`

*  run `socialnetworkcc` 

The program will look for a file named **network.json** that contains the mock data of the social network.

The JSON structure is constructed as follows:

`{ "user1": ["user2","user3"],
  "user2": ["friend1", "user3"],
  "user3": ["user1", "user2"]
}`

Each element represents an user and its friends list.
In this example we can see that **user1** is friend of **user2** and **user3.**

This program looks for relationships between users in the network and find if there is a route
from one user to other.

The program will ask for **username1** and **username2**
and would check if the users are connected through their friends list.

Program output:
- Search duration
- If the users are connected
- How many users it had to traverse to find the result

Then after presenting the results the program will ask if you want to continue and make a new query.
At this point if the user inputs "n" in the console the program will end, any other input make the program continue.

**There is an optional variable in main.go for printing the route used to find the result.*

`var printRoute = false`

Additionally there are some test in the main_test.go that you can run inside the project directory with
`go test`









