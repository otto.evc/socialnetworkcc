package main

import (
	"testing"
)

//for this exercise i'm assuming a connection between the node and itself
// exist even if not described explicitly in the data so {"user1" : ["user1"]} is the same as {"user1":[]}
func Test_findRelationshipSameNode(t *testing.T) {

	network := map[string][]string{"bob": {}}
	route := make([]string, 0, 100)
	nodeA := "bob"
	nodeB := "bob"

	_, jumps, result := findRelationship(network, route, nodeA, nodeB)

	if jumps != 0 || result != true {
		t.Errorf("Error")
	}
}

//return should be 1 for this example as the route from 1 to 3 doesn't exist but the search must go to 2 to find out.
func Test_findRelationshipNoRelation(t *testing.T) {

	network := map[string][]string{
		"1": {"2"},
		"2": {"1"},
		"3": {"4"},
		"4": {"3"}}

	route := make([]string, 0, 100)
	nodeA := "1"
	nodeB := "3"

	_, jumps, result := findRelationship(network, route, nodeA, nodeB)

	if jumps == 1 || result != false {
		t.Errorf("Error")
	}
}

//Testing against loops and the order of the friends in the map
func Test_findRelationshipLoops(t *testing.T) {

	network := map[string][]string{
		"1": {"2", "3"},
		"2": {"1", "3"},
		"3": {"1", "2"}}
	route := make([]string, 0, 100)
	nodeA := "1"
	nodeB := "3"

	_, jumps, result := findRelationship(network, route, nodeA, nodeB)

	if jumps != 1 || result != true {
		t.Errorf("Error")
	}
}

//testing against a non existing node in the network should always return false
func Test_findRelationshipNonExistantNode(t *testing.T) {

	network := map[string][]string{
		"1": {"2", "3"},
		"2": {"1", "3"},
		"3": {"1", "2"}}
	route := make([]string, 0, 100)
	nodeA := "1"
	nodeB := "463"

	_, jumps, result := findRelationship(network, route, nodeA, nodeB)

	if jumps != 0 || result != false {
		t.Errorf("Error")
	}
}

//checking if both directions give the same result
//with this data the results should be the same
func Test_findRelationship9JumpsBidirectional(t *testing.T) {

	network := map[string][]string{
		"1":  {"2"},
		"2":  {"1", "3"},
		"3":  {"2", "4"},
		"4":  {"3", "5"},
		"5":  {"4", "6"},
		"6":  {"5", "7"},
		"7":  {"6", "8"},
		"8":  {"7", "9"},
		"9":  {"8", "10"},
		"10": {"9"}}

	route := make([]string, 0, 100)
	nodeA := "10"
	nodeB := "1"

	route2 := make([]string, 0, 100)
	nodeA2 := "1"
	nodeB2 := "10"

	_, jumps, result := findRelationship(network, route, nodeA, nodeB)
	_, jumps2, result2 := findRelationship(network, route2, nodeA2, nodeB2)

	if jumps != 9 || result != true {
		t.Errorf("Error")
		println(jumps)
		println(result)
	}

	if jumps2 != 9 || result2 != true {
		t.Errorf("Error")
		println(jumps)
		println(result)
	}

}

//checking if both directions give the same result
//with this data the results should be different as we added 10 and 2
// as friends but the search is based on the order of the friends in the friend list
func Test_findRelationship9JumpsBidirectionalWrongData(t *testing.T) {

	network := map[string][]string{
		"1":  {"2"},
		"2":  {"1", "3", "10"},
		"3":  {"2", "4"},
		"4":  {"3", "5"},
		"5":  {"4", "6"},
		"6":  {"5", "7"},
		"7":  {"6", "8"},
		"8":  {"7", "9"},
		"9":  {"8", "10"},
		"10": {"9", "2"}}

	route := make([]string, 0, 100)
	nodeA := "10"
	nodeB := "1"

	route2 := make([]string, 0, 100)
	nodeA2 := "1"
	nodeB2 := "10"

	_, jumps, result := findRelationship(network, route, nodeA, nodeB)
	_, jumps2, result2 := findRelationship(network, route2, nodeA2, nodeB2)

	if jumps != 9 || result != true {
		t.Errorf("Error")
		println(jumps)
		println(result)
	}

	if jumps2 != 2 || result2 != true {
		t.Errorf("Error")
		println(jumps)
		println(result)
	}

}

//testing if direct jump optimization works
func Test_findRelationshipdirectJump(t *testing.T) {

	network := map[string][]string{
		"1":  {"2", "10"},
		"2":  {"1", "3"},
		"3":  {"2", "4"},
		"4":  {"3", "5"},
		"5":  {"4", "6"},
		"6":  {"5", "7"},
		"7":  {"6", "8"},
		"8":  {"7", "9"},
		"9":  {"8", "10"},
		"10": {"1", "9"}}

	route := make([]string, 0, 100)
	nodeA := "1"
	nodeB := "10"

	_, jumps, result := findRelationship(network, route, nodeA, nodeB)

	if jumps != 1 || result != true {
		t.Errorf("Error")
		println(jumps)
		println(result)
	}
}
