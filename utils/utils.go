package utils

import (
	"fmt"
	"io/ioutil"
	"os"
)

// checks if a string Array contains the string
func Contains(arr []string, str string) bool {
	for _, v := range arr {
		if str == v {
			return true
		}
	}
	return false
}

// read the file in the specified location and returns a byte array
func ReadFile(location string) []byte {

	file, err := os.Open(location)

	if err != nil {
		fmt.Println(err)
		fmt.Println("Please check your file location ")

	}

	if file != nil {
		fBytes, err := ioutil.ReadAll(file)
		if err != nil {
			fmt.Println(err)
		}
		defer file.Close()
		return fBytes
	} else {
		return nil
	}

}
