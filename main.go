package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"socialNetworkProject/utils"
	"strings"
	"time"
)

const location = "network.json"

var network map[string][]string
var printRoute = false
var dataLoaded = false

// loads the social network Data to map from a json in the specified location
func loadSocialNetwork() map[string][]string {

	//Map to store network Users and Friends
	var network map[string][]string

	err := json.Unmarshal(utils.ReadFile(location), &network)

	if err != nil {
		fmt.Println(err)
		fmt.Println("Please check your file ")
		return nil
	} else {
		return network

	}

}

// We try to find a route between nodes starting from the nodeA and ending in nodeB.
//we use the network map, the current route and the nodes as parameters
//we return last route used, the amount of jumps and the boolean result for the lookup
func findRelationship(network map[string][]string, route []string, nodeA string, nodeB string) ([]string, int, bool) {

	// validating if the nodeA and nodeB exist within the network map, if one of them doesnt
	// exist we know there is no route between them so they are not related.
	if _, exist := network[nodeA]; !exist {
		return route, len(route), false
	}
	if _, exist := network[nodeB]; !exist {
		return route, len(route), false
	}

	route = append(route, nodeA)

	//if nodeA and nodeB are the same we return true so we dont waste time looking,
	//in a real world exercise this would depend on implementation and what type and of graph is being used to represent
	// the network, for this exercise i'm assuming that an implicit connection between the node and itself
	// exist even if not described explicitly in the data so {"user1" : ["user1"]} is the same as {"user1":[]}
	if nodeA == nodeB {
		return route, len(route) - 1, true
	}

	//direct jump if friend
	//if the nodeB is in nodeA friend list we know for sure that they are connected so we use this shortcut to end the search.
	if utils.Contains(network[nodeA], nodeB) {
		for _, node := range network[nodeA] {
			if node != nodeB {
				continue
			} else {
				route = append(route, node)
				return route, len(route) - 1, true
			}
		}

	} else {
		//we check all the friends in nodeA and apply recursion to check if there is a route between this friend node and the end node.
		for _, node := range network[nodeA] {
			//this check is here to avoid loops and passing through the same node twice.
			if !utils.Contains(route, node) {
				newRoute, _, result := findRelationship(network, route, node, nodeB)

				//if a route was found return the route
				if len(newRoute) > 0 {
					//return  newRoute, len(newRoute)-1,result
					return newRoute, len(newRoute) - 1, result
				}
			}
		}
	}

	//default return false and empty array
	return make([]string, 0, 100), len(route) - 1, false

}

func init() {
	//Load Data
	network = loadSocialNetwork()

}

func main() {
	//initialize variables
	var path = make([]string, 0, 100)
	stop := false

	if network != nil {
		dataLoaded = true
	}

	if dataLoaded != false {
		//to capture input from console until stop is true
		for ok := true; ok; ok = !stop {
			fmt.Println("\nSocial Network Friend Finder")
			fmt.Println("Find out if the 2 users are connected via their friend list")

			reader := bufio.NewReader(os.Stdin)
			fmt.Print("Enter username1: ")
			startNode, _ := reader.ReadString('\n')
			startNode = strings.TrimSuffix(startNode, "\n")

			fmt.Print("Enter username2: ")
			endNode, _ := reader.ReadString('\n')
			endNode = strings.TrimSuffix(endNode, "\n")

			startTime := time.Now()
			routeQuery, jumps, result := findRelationship(network, path, startNode, endNode)
			elapsed := time.Since(startTime)
			fmt.Println("query time = ", elapsed)
			fmt.Println("Users are connected? = ", result)
			fmt.Println("how many users it had to traverse to find the answer = ", jumps)
			if printRoute {
				fmt.Printf("Route from %v to %v = %v \n", startNode, endNode, routeQuery)
			}

			fmt.Print("\n")
			fmt.Print("Continue (y/n): ")
			running, _ := reader.ReadString('\n')
			running = strings.TrimSuffix(running, "\n")
			if running == "n" {
				stop = true
			}

			path = make([]string, 0, 100)
			result = false
			jumps = 0

		}
	}

}
